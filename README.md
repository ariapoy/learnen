# learnen

Learn English with news, video and articles.

## Resource

- [VoiceTube](https://tw.voicetube.com)
- [Learning English - BBC](http://www.bbc.co.uk/learningenglish/)
- [CNN 10](https://edition.cnn.com/cnn10)
- [VOA Learning English](https://learningenglish.voanews.com)
- [Duolingo](https://www.duolingo.com)

## How

1. Learning process on **VoiceTube**

```mermaid
graph TD
  subgraph main process
  channel/bbc==>all/2(all/mediam)==>everyday/date
  everyday/date==>channel/ielts
  channel/ielts==>channel/speech
  channel/ielts==>channel/educational_videos
  style everyday/date fill:#f9f,stroke:#333,stroke-width:4px
  end
  subgraph detail process
  Listen-->Write-->Read-->Speak
  end
```

2. Learning process on **Duolingo**

- Pass the test as many as possible

3. Learning process on **News website**

```mermaid
graph TD
  subgraph main process
  BBC===CNN
  CNN===VOA
  VOA===BBC
  end
  subgraph detail process
  Listen-->Write-->Read-->Speak
  end
```

4. Note

- About 2 ~ 3 hours time box.
- Everyday with a little step.
- Every month with a [test](https://learningenglish.voanews.com/a/4272901.html).